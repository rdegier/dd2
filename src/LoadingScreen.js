import Phaser from 'phaser'

export default class LoadingScreen extends Phaser.Scene {
  constructor () {
    super('loading-screen')
  }

  preload () {

  }

  create () {
    this.add.text(this.scale.width / 2 - 200, this.scale.height / 2, "We are now 'loading'...... press space again to finish loading")
    window.global.keyboard = this.input.keyboard.createCursorKeys()
  }

  update () {
    // We declare the variable in GlobVars and implement it in MainMenue
    if (window.global.keyboard.space.isDown) {
      this.scene.start('game-scene')
    }
  }
}
