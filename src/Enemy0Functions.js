import { Enemy, EnemyGroup } from './Enemy'

const groupConfig = {
  classType: Phaser.GameObjects.Sprite,
  defaultKey: null,
  defaultFrame: null,
  active: true,
  visible: true,
  maxSize: -1,
  runChildUpdate: false,
  createCallback: null,
  removeCallback: null,
  createMultipleCallback: null
}

//* ****************Enemy 0****************************************************** */
// This function is used to load the sprites for the animation of the Enemy0-Element in the GameScene.js
function LoadEnemy0 () {
  // The placeholder-image for the enemyanimation. The Hitbox will be of the size of the placeholder.
  // The hitbox can however been set manually in the EnemyCreator
  window.global.gameScene.load.image('enemy_0', 'EnemySpawner/Enemy0_vertical.png')
  // The spriteatlas and the .json for the enemyanimation.
  window.global.gameScene.load.atlas('enemy0', 'Spritesheets/Enemies/Enemy0_sprite.png', 'Spritesheets/Enemies/Enemy0_sprite.json')
  window.global.gameScene.load.atlas('idle', 'Spritesheets/idle.png', 'Spritesheets/idle.json')
}
// The function can now be globally called by: LoadEnemy0
window.LoadEnemy0 = LoadEnemy0

// .....................

// This function creates the animation of the Enemy0-Column-Element (a column-segment) of an Enemy0 in the GameScene.js
// of all Enemy0-Column-Elements in the Game-Scene.
function AnimationEnemy0 () {
  // Animations are globally created and can be accessed by any sprite afterwards.
  // The subsequent animation can therefore be accessed also by other gameObjects than physicalEnemy.
  window.global.gameScene.anims.create({
    key: 'playEnemy0',
    frameRate: 9.1,
    // @ts-ignore
    // The key 'enemy0' is from the LoadEnemy0()-Function, the atlas where the images are stored
    frames: window.global.gameScene.anims.generateFrameNames('enemy0', {
      // For the Prefix take the names as defined in the atlas .json-script
      prefix: 'Body_Vertical_',
      suffix: '.png',
      start: 1,
      end: 6
    }),
    repeat: 0
  })
}
window.AnimationEnemy0 = AnimationEnemy0

// This function creates the animation of the Enemy0-Column-Element (a column-segment) of an Enemy0 in the GameScene.js
// of all Enemy0-Column-Elements in the Game-Scene.
function AnimationEnemyHead0 () {
  // Animations are globally created and can be accessed by any sprite afterwards.
  // The subsequent animation can therefore be accessed also by other gameObjects than physicalEnemy.
  window.global.gameScene.anims.create({
    key: 'playEnemyHead0',
    frameRate: 9.1,
    // @ts-ignore
    // The key 'enemy0' is from the LoadEnemy0()-Function, the atlas where the images are stored
    frames: window.global.gameScene.anims.generateFrameNames('enemy0', {
      // For the Prefix take the names as defined in the atlas .json-script
      prefix: 'Head_Vertical_',
      suffix: '.png',
      start: 1,
      end: 24
    }),
    repeat: -1
  })
}
window.AnimationEnemyHead0 = AnimationEnemyHead0

// ------------------------------------------------------------------------------------------

// An enemy of Type 0 with one column-element and one head-element.
// This function will be used as creator() for Objects of Spawner or EnemySpawner
function goEnemy0CreatorTier1 () {
  // The array to hold the columns of the Enemytype
  const enemyTier1 = new EnemyGroup(groupConfig)
  enemyTier1.type = 0
  // We create the elements first.

  const element1 = goEnemy0Creator()
  // let element2=goEnemy0Creator();
  const head = goEnemy0HeadCreator()

  // We give the Elements a reference to their group
  head.group = enemyTier1
  element1.group = enemyTier1
  // The physical Bodies are grouped together in an
  enemyTier1.add(element1)
  // enemyTier1.add(element2);
  enemyTier1.add(head)

  // We set the collision enemy-group and player is created.
  const physicalPlayer = window.global.player.player
  window.global.gameScene.physics.add.collider(physicalPlayer, enemyTier1, hitEnemy)

  // Then we place the grouped elements, the initialisation of the Position
  // will be done in initializer0Enemy
  return enemyTier1
}
window.goEnemy0CreatorTier1 = goEnemy0CreatorTier1

// .................

function goEnemy0CreatorTier2 () {

}
window.goEnemy0CreatorTier2 = goEnemy0CreatorTier2

// ................

// This one creates the column-Element of an Enemy of type0
function goEnemy0Creator () {
  // The image will be added as sprite. An global animation can now be loaded
  // into this sprite by .play.
  const sceneCreate = window.global.gameScene
  const enemy = new Enemy(sceneCreate, 900, 300, 'enemy_0')
  console.log('enemytext1: ' + enemy)
  // The type of the Enemy is 0. Physics are added to a sprite and the sprite is saved.
  // The key 'enemy0' is from the LoadEnemy0()
  // enemy=sceneCreate.physics.add.sprite(900, 300, 'enemy_0')
  enemy.setScale(1.25, 1.25)
  enemy.setCollideWorldBounds(true, 0, 0)
  enemy.body.setGravityY(300)
  enemy.body.setAllowGravity(false)
  enemy.body.setVelocityY(0)
  enemy.body.setVelocityX(0)
  // Here we tag the Enemy with the type it belongs to.
  enemy.type = 0
  // enemy.physicalEnemy.body.setBounce(1)

  // The position of the Enemy is fixed.
  // enemy.physicalEnemy.body.moves=false;
  return enemy
}
window.goEnemy0Creator = goEnemy0Creator

// ....................This one creates the heads
// This one creates the head-Element of an Enemy of type0
// This function is only useful, if we want, that the physics of the head act differently.
// otherwise we can just create a goEnemy0Creater-column object with a Head-Animation
function goEnemy0HeadCreator () {
  // The image will be added as sprite. An global animation can now be loaded
  // into this sprite by .play
  const sceneCreate = window.global.gameScene
  const enemy = new Enemy(sceneCreate, 900, 300, 'enemy_0')
  // The type of the Enemy is 0. Physics are added to a sprite and the sprite is saved.
  // enemy=sceneCreate.physics.add.sprite(900, 300, 'enemy_0')

  enemy.setCollideWorldBounds(true, 0, 0)
  enemy.body.setGravityY(300)
  enemy.body.setAllowGravity(false)
  enemy.body.setVelocityY(0)
  enemy.body.setVelocityX(0)
  // Here we tag the Enemy with the type it belongs to.
  enemy.type = 0
  // enemy.physicalEnemy.body.setBounce(1)

  // The position of the Enemy is fixed.
  // enemy.physicalEnemy.body.moves=false;
  return enemy
}
window.goEnemy0HeadCreator = goEnemy0HeadCreator

// ------------------------------------------------------------------------------------------

// Moves all Types of Enemies to the right position.
function initializer0Enemy (enemyIn) {
  // Places and activates Enemy, when Enemy is Spawn
  // enemyIn.physicalEnemy.x=window.global.player.player.x+window.global.playerSpawndist
  enemyIn.setX(window.global.playerStartX + window.global.playerSpawndist)
  enemyIn.setY(window.global.playerStartY)
  let i
  console.log('groupgroesse: ' + enemyIn.getLength())

  for (i = 0; i < enemyIn.getLength() - 2; i++) {
    console.log('positioning')
    enemyIn.getChildren()[0 + i].setY(window.global.playerStartY + (enemyIn.getLength() - i - 1) * window.global.catPileGap)
    const test = enemyIn.getChildren()[i]
    console.log('test: ' + test)
  }
  // Place the head
  enemyIn.getChildren()[enemyIn.getLength() - 1].setY(window.global.playerStartY - window.global.catHeadGap)
}
window.initializer0Enemy = initializer0Enemy

// ------------------------------------------------------------------------------------------

// The Callbackfunction has to be defined locally, since it cannot have variables in the Parantheses
// When it is called in the isCollided-Variable.
function hitEnemy (playerIn, enemyIn) {
  // Here for example fitting methods of dummy and
  // player could be called.
  console.log('Collision')
  // We set the gravity for each child of this group active
  enemyIn.group.getChildren().forEach(function (child) { child.body.setAllowGravity(true) }, this)
  window.global.gameOver = true
}
