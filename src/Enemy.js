
import Phaser from 'phaser'
import Spawner from './Spawner'

// Used to create an EnemySpawner for Types of Enemies.
export class EnemySpawner extends Spawner {
  constructor (goEnemy0Creator, goEnemy0Initializer, noOfTiers) {
    super(goEnemy0Creator, goEnemy0Initializer)
    this.noOfTiers = noOfTiers
  }
}

// An Enemy with Physics
export class Enemy extends Phaser.Physics.Arcade.Sprite {
  // visibility and activeness of every single GameObject ca be handelt by config-data.
  // https://rexrainbow.github.io/phaser3-rex-notes/docs/site/group/
  // Every Enemy knows its Index in the Array and the scene to belong to.
  // Every Enemy knows of what type it is.
  constructor (sceneIn, positionX, positionY, enemySprite) {
    super(sceneIn, positionX, positionY, enemySprite)
    window.global.gameScene.add.existing(this)
    window.global.gameScene.physics.add.existing(this)
    this.IndexEnemy = 0
    this.type = 0
    // reference to the group, which the enemy belongs to.
    this.group = null
  }

  MoveEnemy () {
    if (keyboard.space.isDown) {
      this.x += window.global.playerVelocity
    }
  }
}

// When e.g. the Columnparts and the Head of a Cat are stiched together, they form  a group
export class EnemyGroup extends Phaser.GameObjects.Group {
  // https://rexrainbow.github.io/phaser3-rex-notes/docs/site/group/
  // By the group it is possible to set a group visible or invisible,
  // active or passive, and change its position.
  constructor (configIn) {
    super(configIn)
    window.global.gameScene.add.existing(this)
    this.type = 0
  }

  // Depending on the Enemytype different Animations are played.
  PlayEnemyGroup () {
    switch (this.type) {
      case 0:
        // this.playAnimation('playEnemyHead0',true)

        for (let i = 0; i < this.getLength() - 1; i++) {
          this.getChildren()[i].anims.play('playEnemy0', true)
        }

        this.getChildren()[this.getLength() - 1].anims.play('playEnemyHead0', true)

        break
      case 1:
        break
      default:
    }
  }

  MoveEnemyGroup () {
  }
}
