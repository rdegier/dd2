
import Phaser from 'phaser'

let physicalPlayerDummy

// Reminder:
export default class PlayerDummy extends Phaser.Scene {
  constructor (sceneIn) {
    super('dummy')
    this.sceneForDummy = sceneIn
  }

  preload () {
    // "this" is the player.
    this.sceneForDummy.load.image('starasset', '/EnemySpawner/star.png')
  }

  create () {
    physicalPlayerDummy = this.sceneForDummy.physics.add.image(690, 410, 'starasset')
    // window.global.player = this.add.image(100,450, 'starasset');
    // Collides with the window, which is enclosing the game.
    physicalPlayerDummy.setCollideWorldBounds(true)
    physicalPlayerDummy.body.allowGravity = false
    // physicalPlayerDummy.body.setGravityY(0);
  }

  getPhysicalPlayerDummy () {
    return physicalPlayerDummy
  }
}
