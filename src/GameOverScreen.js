import Phaser from 'phaser'

var container

export default class GameOverScreen {
  constructor (sceneIn) {
    this.scene = sceneIn

    this.container = container
  }

  preload () {
    this.scene.load.image('crashedpopup', 'GameOverScreen/crashed_popupwindow.png')
    this.scene.load.image('surroundingwindow', 'GameOverScreen/surroundingWindow.png')
    this.scene.load.image('revivetext', 'GameOverScreen/ReviveText.png')
    this.scene.load.image('lifebutton', 'GameOverScreen/LifeButton.png')
    this.scene.load.image('lifebuttonhighlighted', 'GameOverScreen/LifeButtonHighlighted.png')
    this.scene.load.image('nextbutton', 'GameOverScreen/NextButton.png')
    this.scene.load.image('nextbuttonhighlighted', 'GameOverScreen/NextButtonHighlighted.png')
    this.scene.load.image('heartfishicon', 'GameOverScreen/icon_heartfish.png')
  }

  create () {
    var containerPosY = this.scene.scale.height * 0.45
    this.container = this.scene.add.container(0, containerPosY)

    // container items (position relative to container)
    var popupWindow = this.scene.add.image(0, 0, 'crashedpopup')
    var windowWidth = popupWindow.width
    var windowHeight = popupWindow.height
    var reviveText = this.scene.add.image(0, -(windowHeight * 0.5), 'revivetext')
    reviveText.angle = -5
    var nextButtonBox = this.scene.add.image(0, windowWidth * 0.66, 'surroundingwindow')
    nextButtonBox.setDisplaySize(nextButtonBox.width * 0.3, nextButtonBox.height * 0.3)
    var heartFishIcon = this.scene.add.image(windowWidth * 0.25, -(windowHeight * 0.35), 'heartfishicon')
    heartFishIcon.setDisplaySize(heartFishIcon.width * 0.33, heartFishIcon.height * 0.33)

    // buttons
    var nextButton = this.scene.add.image(0, windowHeight * 0.66, 'nextbutton')
    nextButton.setInteractive()
    nextButton.on('pointerdown', () => { this.scene.scene.start('game-over') })

    var lifeButton = this.scene.add.image(-(windowWidth * 0.21), windowHeight * 0.28, 'lifebutton')
    lifeButton.setInteractive()

    this.container.add([popupWindow, reviveText, nextButtonBox, nextButton, lifeButton, heartFishIcon])
    this.container.depth = 1
    this.container.alpha = 0 // invisible
  }

  SetActive () {
    // updates x position of UI
    this.container.x = this.scene.cameras.main.worldView.x + this.scene.scale.width / 2

    if (window.global.gameOver) {
      this.container.alpha = 1
      window.global.player.setVelocityX(0)
      window.global.player.setVelocityY(0)
    }
  }
}
