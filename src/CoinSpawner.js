import Spawner from './EnemyFolder/Spawner'
import Coin from './Coin'

export default class CoinSpawner extends Spawner {
  constructor (scene) {
    super(scene,
      (scene) => new Coin(scene),
      (coin) => {
      },
      (coin) => {
        return coin.x > 0
      })
  }
}
