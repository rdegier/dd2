
import Phaser from 'phaser'
import Enemy0 from './Enemy0'

// Reminder:
export default class EnemySpawner {
  constructor (sceneIn) {
    this.sceneForEnemy = sceneIn
    // There will be much more Enemytypes than just one!
    this.numberOfEnemyTypes = 1
  }

  // An Enemy is Spawn according to its typenumber
  SpawnEnemy (enemyType, index) {
    let newEnemy
    switch (enemyType) {
      case 0: newEnemy = new Enemy0(this.sceneForEnemy, index)
        newEnemy.CreatePhysicalEnemy()
        break
      case 1:
        break
      case 2: break
      default:
    }
    return newEnemy
  }

    l
    LoadEnemyInfo () {
      const loadEnemy0 = new Enemy0(this.sceneForEnemy, 0)
      loadEnemy0.LoadEnemy()
    }
}
