// import Enemy0 from './Enemy0'
import Spawner from './Spawner'

// Reminder:
export default class EnemySpawner extends Spawner {
  constructor (sceneIn) {
    super(sceneIn, null, (go) => {
      go.x = 0
    }, (scene, go) => {
      return go.x > 0
    })
    this.sceneForEnemy = sceneIn
    // There will be much more Enemytypes than just one!
    this.numberOfEnemyTypes = 1
  }
}
