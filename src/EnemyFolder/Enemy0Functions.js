// .....................

// This function creates the animation of the Enemy0-Column-Element (a column-segment) of an Enemy0 in the GameScene.js
// of all Enemy0-Column-Elements in the Game-Scene.
function AnimationEnemy (keyIn, playKey, prefixIn, suffixIn, startIn, endIn) {
  // Animations are globally created and can be accessed by any sprite afterwards.
  // The subsequent animation can therefore be accessed also by other gameObjects than physicalEnemy.
  // console.log('keyIn: ' + keyIn + '  playKey: ' + playKey + '  prefixIn ' + prefixIn + '  suffixIn ' + suffixIn + '  startIn ' + startIn + '  endIn ' + endIn + '')
  // keyIn: enemy0Upsidedown  playKey: playEnemy1  prefixIn EUpsidedown_Body_  suffixIn .png  startIn 1  endIn 6
  return window.global.gameScene.anims.create({
    key: playKey,
    frameRate: 30,
    // @ts-ignore
    // The key 'enemy0' is from the LoadEnemy0()-Function, the atlas where the images are stored
    frames: window.global.gameScene.anims.generateFrameNames(keyIn, {
      // For the Prefix take the names as defined in the atlas .json-script
      prefix: prefixIn,
      suffix: suffixIn,
      start: startIn,
      end: endIn
    }),
    repeat: -1
  })
}
window.AnimationEnemy = AnimationEnemy

// ................

// This one creates the column-Element of an Enemy of type0
function goEnemy0Creator () {
  // The image will be added as sprite. An global animation can now be loaded
  // into this sprite by .play.
  const sceneCreate = window.global.gameScene
  const enemy = sceneCreate.physics.add.sprite(690, 410, 'enemy_0')
  // The type of the Enemy is 0. Physics are added to a sprite and the sprite is saved.
  // The key 'enemy0' is from the LoadEnemy0()
  // enemy=sceneCreate.physics.add.sprite(900, 300, 'enemy_0')
  enemy.setScale(1.0, 1.0)
  // enemy.setCollideWorldBounds(true, 0, 0)
  enemy.body.setGravityY(300)
  enemy.body.setAllowGravity(false)
  enemy.body.setVelocityY(0)
  enemy.body.setVelocityX(0)
  // Here we tag the Enemy with the type it belongs to.
  enemy.type = 0
  // enemy.physicalEnemy.body.setBounce(1)

  // The position of the Enemy is fixed.
  // enemy.physicalEnemy.body.moves=false;
  return enemy
}
window.goEnemy0Creator = goEnemy0Creator
// ------------------------------------------------------------------------------------------

// Moves all Types of Enemies to the right position.
function initializer0Enemy (enemyIn) {
  // Places and activates Enemy, when Enemy is Spawn
  // enemyIn.physicalEnemy.x=window.global.player.player.x+window.global.playerSpawndist
  enemyIn.setX(window.global.playerStartX + window.global.playerSpawndist)
  enemyIn.setY(window.global.playerStartY)
  let i

  for (i = 0; i < enemyIn.getLength() - 2; i++) {
    enemyIn.getChildren()[0 + i].setY(window.global.playerStartY + (enemyIn.getLength() - i - 1) * window.global.catPileGap)
    const test = enemyIn.getChildren()[i]
    // console.log('test: ' + test)
  }
  // Place the head
  enemyIn.getChildren()[enemyIn.getLength() - 1].setY(window.global.playerStartY - window.global.catHeadGap)
}
window.initializer0Enemy = initializer0Enemy

// ------------------------------------------------------------------------------------------

// The Callbackfunction has to be defined locally, since it cannot have variables in the Parantheses
// When it is called in the isCollided-Variable.
function hitEnemy (playerIn, enemyIn) {
  // Here for example fitting methods of dummy and
  // player could be called.
  enemyIn.group.getChildren().forEach(function (child) {
    child.body.setAllowGravity(true)
    child.body.angularVelocity = -60
    child.body.angularDrag = 45
    child.body.setVelocityX(0)
  }, this)

  // somehow, the collider can't be deleted anymore.
  // enemyIn.group.enemyGroupCollider.destroy()

  window.global.gameOver = true
}
window.hitEnemy = hitEnemy
