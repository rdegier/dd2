import Phaser from 'phaser'

// When e.g. the Columnparts and the Head of a Cat are stiched together, they form  a group
export class EnemyGroup extends Phaser.GameObjects.Group {
  // https://rexrainbow.github.io/phaser3-rex-notes/docs/site/group/
  // By the group it is possible to set a group visible or invisible,
  // active or passive, and change its position.
  constructor (configIn) {
    super(configIn)
    window.global.gameScene.add.existing(this)
    this.section = null
    this.type = 100
  }

  // Depending on the Enemytype different Animations are played. See EnemyGroup.js
  // this.generationFunctions = [
  //   Section.SimpleSection,
  //   Section.SimpleCloseSection,
  //   Section.TallSection,
  //   Section.TopSection,
  //   Section.CloseTopSection,
  //   Section.VerticalMover,
  //   Section.MultipleColumns,
  //   Section.VerticalMoversRanged,
  //   Section.VerticalMoversRangedAndOffset,
  //   Section.Floaters,
  //   Section.CloseTopSectionPlusPlus,
  //   Section.DoubleColumnFloaters,
  //   Section.FloatersHard,
  //   Section.RandomDifficulty
  // ]
  // constructor (nbEnemies, dist, top, verticalMoveRange, verticalOffset, nbColumns, gapSize = 1.1, verticalSpeed = 2, growing = false, horizontalSpeed = 0) {

  // Simple, SimpleClose and Tall sections can all be played with the same play function.
  // static SimpleSection () { return new Section(Math.randRange(2, 4), Math.randRange(4.0, 10.0), false, 0, 0, 1) }

  PlayEnemyGroup () {
    // ------------------------------------- Column-Enemies ---------------------------------------
    if ((!this.section.top) && (this.section.verticalMoveRange === 0) && (this.section.nbColumns < 2)) {
      if (this.section.horizontalSpeed >= 0) {
        this.type = 0 // not moving
      } else {
        this.type = 1 // moving
      }
    }

    if ((this.section.top) && (this.section.verticalMoveRange === 0) && (this.section.nbColumns < 2)) {
      if (this.section.horizontalSpeed >= 0) {
        // console.log('*************************** is upside down*******************************')
        this.type = 2
      } else {
        this.type = 3 // moving upside}
      }
    }
    // ---------------------------------- End Column-Enemies --------------------------------------

    // ------------------------------------- Grid-Enemies ---------------------------------------
    if ((this.section.verticalMoveRange === 0) && (this.section.nbColumns > 1)) {
      if (this.section.horizontalSpeed >= 0) {
        this.type = 4
      } else {
        this.type = 5 // moving}
      }
    }
    // ------------------------------------- End Grid-Enemies ---------------------------------------

    switch (this.type) {
      case 0:
        // A simple section, without multiple columns etc., where the last entry should be used as head.
        // For the case that the group is active:
        // console.log('Are the children Active?: ' + this.getChildren()[0].active)
        if (this.getChildren()[0].active === true) {
          for (let i = 0; i < this.getLength() - 1; i++) {
            this.getChildren()[i].setScale(0.6, 0.6)
            this.getChildren()[i].anims.play('playEnemy0', true)
          }
          this.getChildren()[this.getLength() - 1].setActive(true)
          this.getChildren()[this.getLength() - 1].setScale(0.5, 0.5)
          this.getChildren()[this.getLength() - 1].anims.play('playEnemyHead0', true)
        }
        break
      case 1:
        // Moving blue enemy
        if (this.getChildren()[0].active === true) {
          for (let i = 0; i < this.getLength() - 1; i++) {
            this.getChildren()[i].setScale(0.6, 0.6)
            this.getChildren()[i].anims.play('playEnemy1', true)
          }
          this.getChildren()[this.getLength() - 1].setActive(true)
          this.getChildren()[this.getLength() - 1].setScale(0.5, 0.5)
          // this.getChildren()[this.getLength() - 1].anims.play('playEnemy1Head', true)
          if (this.getChildren().length > 1) {
          }
        }
        break
      case 2:
        // Upside down enemy
        // console.log('***************************upside down*******************************')
        if (this.getChildren()[0].active === true) {
          for (let i = this.getLength() - 2; i >= 0; i--) {
            this.getChildren()[i].depth = (this.getLength() - 1 - i)
            this.getChildren()[i].setScale(0.5, 0.5)
            this.getChildren()[i].anims.play('playEnemy0upside', true)
          }
        }
        this.getChildren()[this.getLength() - 1].setActive(true)
        this.getChildren()[this.getLength() - 1].setScale(0.5, 0.5)
        this.getChildren()[this.getLength() - 1].depth = 0
        this.getChildren()[this.getLength() - 1].anims.play('playEnemyHead0upside', true)
        if (this.getChildren().length > 1) {
          // window.global.gameScene.add.text(this.getChildren()[this.getLength() - 2].x, this.getChildren()[this.getLength() - 1].y + 100, 'case 2 nbEnemies' + this.section.nbEnemies + ' dist ' + this.section.dist + ' top ' + this.section.top + ' verticalMoveRange ' + this.section.verticalMoveRange + ' verticalOffset ' + this.section.verticalOffset + ' nbColumns ' + this.section.nbColumns + ' gapSize ' + this.section.gapSize + ' vertical Speed ' + this.section.verticalSpeed + ' growing ' + this.section.growing + ' horizontalSpeed ' + this.section.horizontalSpeed)
        }
        break
      case 3:
        // Upside down enemy1 moving
        // console.log('***************************upside down*******************************')
        if (this.getChildren()[0].active === true) {
          for (let i = this.getLength() - 2; i >= 0; i--) {
            this.getChildren()[i].depth = (this.getLength() - 1 - i)
            this.getChildren()[i].setScale(0.5, 0.5)
            this.getChildren()[i].anims.play('playEnemy1Upside', true)
          }
        }
        this.getChildren()[this.getLength() - 1].setActive(true)
        this.getChildren()[this.getLength() - 1].setScale(0.5, 0.5)
        this.getChildren()[this.getLength() - 1].depth = 0
        // this.getChildren()[this.getLength() - 1].anims.play('playEnemy1HeadUpside', true)
        if (this.getChildren().length > 1) {
          // window.global.gameScene.add.text(this.getChildren()[this.getLength() - 2].x, this.getChildren()[this.getLength() - 1].y + 100, 'case 3 nbEnemies' + this.section.nbEnemies + ' dist ' + this.section.dist + ' top ' + this.section.top + ' verticalMoveRange ' + this.section.verticalMoveRange + ' verticalOffset ' + this.section.verticalOffset + ' nbColumns ' + this.section.nbColumns + ' gapSize ' + this.section.gapSize + ' vertical Speed ' + this.section.verticalSpeed + ' growing ' + this.section.growing + ' horizontalSpeed ' + this.section.horizontalSpeed)
        }
        break
      case 4:
      // An wall-enemyblock which does not move
        if (this.getChildren()[0].active === true) {
          for (let i = 0; i <= this.getLength() - 1; i++) {
            this.getChildren()[i].setScale(0.5, 0.5)
            this.getChildren()[i].anims.play('playEnemy2', true)
          }
        }
        break

      case 5:
        // An wall-enemyblock which does not move
        if (this.getChildren()[0].active === true) {
          for (let i = 0; i <= this.getLength() - 1; i++) {
            this.getChildren()[i].setScale(0.5, 0.5)
            this.getChildren()[i].anims.play('playEnemy3', true)
          }
        }
        break

      default:
    }
  }
}
