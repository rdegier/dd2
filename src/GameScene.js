import Phaser from 'phaser'
import EntityManager from './EntityManager'
import Player from './Player'
import './EnemyFolder/Enemy0Functions'
import GameOverScreen from './GameOverScreen'
import { Vector } from 'matter'
import BackgroundGenerator from './BackgroundGenerator'
import { progressiveLoadfiles, initialLoadfilesGameScene } from './FilesToLoad'

let star
let counter = 0

export default class GameScene extends Phaser.Scene {
  constructor () {
    super('game-scene') // The constructor of a Phaser.Scene has either a string or a config
    // The player is assigned to the GameScene.
    // this.EntityManagerGame = new EntityManager(this)
    this.gameOverScreen = new GameOverScreen(this)
    this.backGroundGenerator = new BackgroundGenerator(this)

    window.global.gameScene = this
    this.stern = null
    this.test = null
    this.testgroup = null
  }

  preload () {
    window.loader1(initialLoadfilesGameScene)

    this.gameOverScreen.preload()
  }

  create () {
    star = this.physics.add.sprite(800, 200, 'star')
    star.body.setAllowGravity(false)
    // The player refences to the global player.
    window.global.player = new Player(this, window.global.playerStartX, window.global.playerStartY)
    window.global.player.CreateAnimation()

    this.gameOverScreen.create()
    this.backGroundGenerator.create()

    // The Animation for the first Enemytype is created (a segment)
    // This can't be done in Preload().
    window.AnimationEnemy('enemy0', 'playEnemy0', 'Body_Vertical_', '.png', 1, 6)
    window.AnimationEnemy('enemy0', 'playEnemyHead0', 'Head_Vertical_', '.png', 1, 24)

    this.physics.world.setFPS(500)
    this.add.text(this.scale.width / 2 - 200, this.scale.height / 2, 'We are now playing the game!')

    // The Player and the first Enemytype are created.
    // window.global.player.create()
    this.entityManager = new EntityManager(this, window.global.player)
    this.add.existing(this.entityManager)
    window.global.keyboard = this.input.keyboard.createCursorKeys()

    this.SetCamera()
  }

  update () {
    window.progressiveLoader2(progressiveLoadfiles)
    window.global.player.Movement()
    window.global.player.PlayAnimation()
    this.entityManager.update()
    this.gameOverScreen.SetActive()
    this.backGroundGenerator.update()

    // All sections are played in each update.
    this.entityManager.sectionsToPlay.forEach(function (sectionOfGroup) {
      sectionOfGroup.PlayEnemyGroup()
    }, this)

    // Play the animation, if the animation exists

    if (this.anims.exists('playEnemy3')) {
      star.anims.play('playEnemy3', true)
    }

    // if (this.textures.exists('img2') && (test === true)) {
    //   console.log('**************************image exists*******************************************')
    //   this.physics.add.image(800, 200, 'img2')
    //   test = false
    // }
    counter++
  }

  SetCamera () {
    this.physics.world.setBounds(0, 0, 10000000, 820)
    this.cameras.main.setBounds(0, 0, 10000000, 820)
    this.cameras.main.startFollow(window.global.player)
    this.cameras.main.setFollowOffset(-window.global.playerCamOffSet, 0)
  }
}
