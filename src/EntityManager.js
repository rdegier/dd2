import Phaser from 'phaser'
import CoinSpawner from './CoinSpawner'
import EnemySpawner from './EnemyFolder/EnemySpawner'
import { EnemyGroup } from './EnemyFolder/EnemyGroup'
import './math.js'
import Vector3 from './Vector3'

const groupConfig = {
  classType: Phaser.GameObjects.Sprite,
  defaultKey: null,
  defaultFrame: null,
  active: true,
  visible: true,
  maxSize: -1,
  runChildUpdate: false,
  createCallback: null,
  removeCallback: null,
  createMultipleCallback: null
}

function MaxColumnsForNumEnemies (nbEnemies) {
  switch (nbEnemies) {
    case 1:
    case 2:
      return 6
    case 3:
      return 4
    case 4:
      return 3
    default:
      return 1
  }
}

class Section {
  /** Section constructor
     * @param {integer} nbEnemies   Number of obstacles
     * @param {float} dist
     * @param {boolean} top
     * @param {float} verticalMoveRange     Range of movement for Floater enemies
     * @param {float} verticalOffset
     * @param {integer} nbColumns           Width of the obstacle
     * @param {float} gapSize
     * @param {float} verticalSpeed         Vertical move speed
     * @param {boolean} growing             Is the enemy a Climber ?
     * @param {float} horizontalMoveSpeed   Horizontal move speed
     */
  constructor (nbEnemies, dist, top, verticalMoveRange, verticalOffset, nbColumns, gapSize = 1.1, verticalSpeed = 2, growing = false, horizontalSpeed = 0) {
    this.nbEnemies = nbEnemies
    this.distToNext = dist
    this.top = top
    this.verticalMoveRange = verticalMoveRange
    this.verticalOffset = verticalOffset
    this.nbColumns = nbColumns
    this.gapSize = gapSize
    this.verticalSpeed = verticalSpeed
    this.growing = growing
    this.horizontalSpeed = horizontalSpeed
  }

  static SimpleSection () { return new Section(Math.randRange(2, 4), Math.randRange(400.0001, 1000), false, 0, 0, 1) }
  static SimpleCloseSection () { return new Section(Math.randRange(2, 5), Math.randRange(300.0001, 500), false, 0, 0, 1) }
  static TallSection () { return new Section(Math.randRange(1, 6), Math.randRange(300.0001, 800), false, 0, 0, 1) }
  static TopSection () { return new Section(Math.randRange(3, 5), Math.randRange(300.0001, 600), Math.random() < 0.5, 0, 0, 1) }
  static CloseTopSection () { return new Section(Math.randRange(1, 6), Math.randRange(250.0001, 800), Math.random() < 0.2, 0, 0, 1) }
  static CloseTopSectionPlusPlus () { return new Section(Math.randRange(1, 6), Math.randRange(250.0001, 800), Math.random() < 0.5, 0, 0, 1) }
  static VerticalMover () { return new Section(Math.randRange(1, 3), Math.randRange(300.0001, 900), false, 3.75, 0, 1, 1.1, Math.randRange(1.5, 3.5)) }
  static MultipleColumns () {
    var nbEnemies = Math.randRange(1, 6)
    return new Section(nbEnemies, Math.randRange(300.0001, 600), false, 0, 0, Math.randRange(1, MaxColumnsForNumEnemies(nbEnemies) + 1))
  }

  static VerticalMoversRanged () {
    var nbEnemies = Math.randRange(1, 3)
    var minRange = nbEnemies * 0.75
    return new Section(nbEnemies, Math.randRange(300.0001, 600), false, Math.randRange(minRange, 1.75), 0, 1, 1.1, Math.randRange(1.5, 3.5))
  }

  static Floaters () {
    var nbEnemies = Math.randRange(1, 3)
    var maxRange = 2.5
    return new Section(nbEnemies, Math.randRange(400.0001, 1000), Math.random() < 0.5, 0, Math.randRange(0.0, maxRange), 1)
  }

  static FloatersHard () {
    var nbEnemies = Math.randRange(1, 3)
    var maxRange = 3.75
    return new Section(nbEnemies, Math.randRange(400.0001, 1000), Math.random() < 0.5, 0, Math.randRange(0.0, maxRange), 1)
  }

  static DoubleColumnFloaters () {
    var nbEnemies = Math.randRange(1, 3)
    var top = Math.random() < 0.25
    var minRange = top ? 1.5 : 0
    var maxRange = 2.5
    return new Section(nbEnemies, Math.randRange(400.0001, 800), top, 0, Math.randRange(minRange, maxRange), 2)
  }

  static VerticalMoversRangedAndOffset () {
    var nbEnemies = Math.biasedLowRandom(1, 4)
    var minRange = nbEnemies * 0.75
    var movementRange = Math.randRange(minRange, 3.75)
    var offsetRange = 3.75 - movementRange
    return new Section(nbEnemies, Math.randRange(400.0001, 1000), false, movementRange, Math.randRange(-offsetRange, offsetRange), 1, 1.1, Math.randRange(1.5, 3.5))
  }

  static RotatingFloaters () {
    var nbEnemies = Math.randRange(2, 4)
    var maxRange = 3.75
    return new Section(nbEnemies, Math.randRange(500.0001, 900), Math.random() < 0.5, 0, Math.randRange(0, maxRange), 1)
  }

  static DoubleVerticalMovers () { return new Section(2, Math.randRange(5.0001, 9), false, 3.75, 0, 1, Math.randRange(3, 4.5), Math.randRange(1.5, 3.5)) }
  static DoubleNonMoving () {
    var nbEnemies = 2
    var nbColumns = Math.randRange(1, 6)
    var gapSize = (nbColumns * 0.75) + Math.randRange(300, 375.001)
    var verticalOffset = 3.75 - gapSize / 2

    if (nbColumns === 1) {
      var extra = Math.random()
      verticalOffset -= extra
      gapSize += extra * 0.75
    }

    return new Section(nbEnemies, Math.randRange(500.0001, 900), false, 0, verticalOffset, nbColumns, gapSize, 0)
  }

  static RandomDifficulty () {
    var nbEnemies = Math.biasedLowRandom(1, 6)
    var moving = nbEnemies <= 3 && Math.random() < 0.5
    var top = !moving && Math.random() < 0.5
    if (top) nbEnemies = Math.biasedLowRandom(3, 7)
    var nbColumns = Math.random() > 0.5 ? 1 : Math.randRange(1, Math.max(1, MaxColumnsForNumEnemies(nbEnemies) + (moving ? -1 : 1)))
    var minVerticalRange = Math.min(1.5 + nbEnemies * nbColumns * 1.0, 3.75)
    var maxDist = (moving || nbColumns >= 2 || top) ? 750 : 950

    return new Section(nbEnemies, Math.randRange(250.0001, maxDist), top, moving ? Math.randRange(minVerticalRange, 3.75) : 0, 0, nbColumns, 1.1, Math.randRange(1.5, 3.5))
  }
}

export default class EntityManager extends Phaser.GameObjects.GameObject {
  constructor (scene, player) {
    console.log('Scene size: ', scene.cameras.main.centerX, scene.cameras.main.centerY)
    super(scene, 'EntityManager')
    this.enemySpawner = new EnemySpawner(scene)
    this.enemySpawner.creator = window.goEnemy0Creator
    this.enemySpawner.activeCheck = (scene, go) => go.x > this.player.x - this.scene.cameras.main.centerX - 200
    this.coinSpawner = new CoinSpawner(scene)
    this.coinSpawner.activeCheck = (scene, go) => go.x > this.player.x - this.scene.cameras.main.centerX - 200
    this.coinGroupId = 0
    this.player = player

    this.initialDistance = 400
    this.genIndex = 0
    this.distance = 0
    this.counter = 0
    // The start of the next Section
    this.nextSection = 0

    this.generationFunctions = [
      Section.SimpleSection, // 0
      Section.SimpleCloseSection, // 1
      Section.TallSection, // 2
      Section.TopSection, // 3
      Section.CloseTopSection, // 4
      Section.VerticalMover, // 5
      Section.MultipleColumns, // 6
      Section.VerticalMoversRanged, // 7
      Section.VerticalMoversRangedAndOffset, // 8
      Section.Floaters, // 9
      Section.CloseTopSectionPlusPlus, // 10
      Section.DoubleColumnFloaters, // 11
      Section.FloatersHard, // 12
      Section.RandomDifficulty // 13
    ]

    this.sectionsToSpawn = [] // Section buffer
    this.sectionsToPlay = [] // Section buffer

    this.nextMarker = 0
    this.nextPickup = 0

    this.GenerateFirstSections()

    scene.anims.create({
      key: 'coin-normal-anim',
      frameRate: 16,
      frames: scene.anims.generateFrameNames('coin-normal', {
        prefix: 'coin_',
        suffix: '.png',
        start: 1,
        end: 24,
        zeroPad: 4
      }),
      repeat: -1
    })
    scene.anims.create({
      key: 'coin-super-anim',
      frameRate: 16,
      frames: scene.anims.generateFrameNames('coin-super', {
        prefix: 'coin_super_',
        suffix: '.png',
        start: 1,
        end: 24,
        zeroPad: 4
      }),
      repeat: -1
    })
  }

  /**
     * Generates the next section of the level
     */
  GenerateNext () {
    var sectionIndex = this.genIndex < 2 ? this.genIndex : Math.randRange(0, Math.min(this.genIndex, 13))
    // console.log('********************Section index******************:' + sectionIndex)
    var numSections = Math.biasedHighRandom(1, Math.max(4, 10 - (this.genIndex / 10)))

    if (sectionIndex === 0) { numSections = Math.randRange(1, 5) }

    for (var i = 0; i < numSections; i++) { this.sectionsToSpawn.push(this.generationFunctions[sectionIndex]()) }

    if (this.genIndex > 12 && Math.random() > 0.66) {
      this.sectionsToSpawn.push(Section.DoubleVerticalMovers())
    } else if (this.genIndex > 6 && Math.random() > 0.66) {
      this.sectionsToSpawn.push(Section.DoubleNonMoving())
    }

    this.genIndex++
  }

  /**
     * Generates the first sections of the level
     */
  GenerateFirstSections () {
    this.nextSection = 10
    this.nextSection = window.global.player.x + this.initialDistance
    this.genIndex = 0

    for (var i = 0; i < 2; i++) { this.GenerateNext() }
  }

  /**
   * Create an Enemy
   * @param {Vector3} position
   * @param {boolean} isHead
   * @param {boolean} flip
   * @param {boolean} hasFeet
   * @param {boolean} hasPropellor
   * @param {float} delay
   * @param {float} horizontalSpeed
   * @param {integer} nbEnemies
   * @param {integer} index
   * @param {boolean} ignoreOffset
   * @param {object} bounds
   * @param {integer} columnIndex
   * @param {boolean} isFloating
   */
  SpawnEnemy (position, isHead, flip, hasFeet, hasPropellor, delay, horizontalSpeed, nbEnemies, index, ignoreOffset, bounds, columnIndex, isFloating) {
    var isMoving = horizontalSpeed < 0
    var step = 0.15

    const enemyPosition = {
      x: 0,
      y: 0,
      z: 0
    }
    // Since the speed is set negative the signatur was changed from +1.5 to -1.5
    enemyPosition.x = position.x - 1.5 * horizontalSpeed
    if (!ignoreOffset) {
      enemyPosition.y = position.y + (isMoving ? -step : step) / (nbEnemies * 2) * (nbEnemies - 1 - index * 2)
      enemyPosition.y += (isMoving ? -step : step) / 2
    }
    enemyPosition.z = position.z + 2 + (isMoving ? -0.2 : 0.2) + position.y * -0.01 + columnIndex * 0.02

    // The spawner creates placeholders for the different animations
    // The function for the column enemy0 is transferred to the creator-function of the Spawner.
    var enemy = this.enemySpawner.spawn()
    enemy.x = enemyPosition.x
    enemy.y = enemyPosition.y
    enemy.z = enemyPosition.z

    // Parts of the functions below could also be done in the EnemyGroup.js
    if (isFloating) {
      // TODO: show floating
    }

    if (hasPropellor) {
      // TODO: add propellor
    }

    if (isMoving) {
      enemy.setVelocity(horizontalSpeed, 0)
    }

    return enemy
  }

  /**
   * Create a coin
   * @param {Vector3} position
   * @param {number} value
   * @param {float} timeOffset
   * @param {float} animateOffset
   * @param {integer} groupId
   */
  SpawnCoin (position, isSuper, timeOffset, animateOffset, index) {
    var coin = this.coinSpawner.spawn()
    coin.x = position.x
    coin.originalY = position.y

    coin.setIsSuper(isSuper)
    coin.timeOffset = timeOffset
    coin.setAnimationDelay(animateOffset)
    coin.setIndex(index)
  }

  /**
   * Creates coins
   * @param {Vector3} position
   */
  SpawnCoins (position) {
    var isSuper = Math.randRange(0.0, 1.00001) < 0.1

    var timeOffset = Math.randRange(0, Math.PI * 2)
    var step = 32
    for (var i = 0; i < 12; i++) {
      var x = -step / 2 + (i % 4) * 50
      var y = -step / 2 + Math.floor(i / 4) * 42
      var pos = position.add(new Vector3(x, y, 2))
      this.SpawnCoin(pos, isSuper, timeOffset + x * 3, (3 - (i % 4)) / 4, i)
    }
  }

  /**
     * Setup a section in the scene
     * @param {Section} section
     * @param {Vector3} position
     */
  SpawnSection (section, position) {
    function IsClimber (section) {
      return section.nbColumns === 1 && section.verticalMoveRange <= 0 && !section.top && 20 - 2.75 + offset <= -3.375 && section.gapSize <= 1.1 && section.horizontalSpeed === 0
    }

    var offset = 0.0
    var maxEnemies = section.top ? 6 : 5
    var nbEnemies = section.nbEnemies

    // TODO: Create checkpoint (lines 460-463)

    if (section.verticalMoveRange <= 0 && this.nextSection > 5000 && Math.random() < 0.5) {
      section.horizontalSpeed = Math.randRange(window.global.enemyHorizontalSpeedHigh, window.global.enemyHorizontalSpeedLow)
    }

    if (this.genIndex <= 6 && nbEnemies >= 4) {
      nbEnemies = 4
      if (section.top) {
        section.horizontalSpeed = 0
        if (this.genIndex <= 4) { nbEnemies = 3 }
      }
    }

    // Can we turn the obstacle into a climber ?
    if (IsClimber(section) && this.genIndex > 3 && Math.random() < 0.25) {
      nbEnemies = 5
      section.growing = true // Otherwise, Limit obstacles
    } else if (nbEnemies >= maxEnemies) {
      nbEnemies = maxEnemies - 1
      offset = section.top ? Math.randRange(0.5, 1) - 1 : Math.randRange(-1, -0.5) + 1
    }

    // Limit obstacles
    if (nbEnemies * section.nbColumns >= 12) {
      if (section.top) { offset = Math.randRange(150, 50) } else {
        offset = Math.randRange(-50, -15) + 110
        nbEnemies--
      }
    }

    var pos = position
    var delay = 0.5

    const enemyGroup = new EnemyGroup(groupConfig)
    let temporaryEnemy
    for (var i = 0; i < nbEnemies; i++) {
      for (var j = 0; j < section.nbColumns; j++) {
        pos.x = position.x + j * 128
        pos.y = (section.top ? 80 * i + 96 : 2 * this.scene.cameras.main.centerY - 80 * i - 140)

        var bounds = {
          left: false,
          right: false,
          top: false,
          bottom: false
        }
        if (section.nbColumns > 1) {
          if (j < section.nbColumns - 1) { bounds.right = true }
          if (j > 0) { bounds.left = true }

          if (section.gapSize <= 1.1) {
            if (section.top) {
              if (i > 0) { bounds.top = true }
              if (i < nbEnemies - 1) { bounds.bottom = true }
            } else {
              if (i > 0) { bounds.bottom = true } else if (i < nbEnemies - 1) { bounds.top = true }
            }
          }
        }

        if (section.verticalMoveRange > 0) {
          var offsetBottom = (section.gapSize * i) + offset
          var offsetTop = (section.gapSize * (nbEnemies * i)) + offset
          var start = 20 + 1 - section.verticalMoveRange + offsetBottom
          var end = 20 + 1 - section.verticalMoveRange - offsetTop + section.gapSize
          start += (section.top ? -1 : 1) * section.verticalOffset
          end += (section.top ? -1 : 1) * section.verticalOffset

          pos.y = Math.lerp(start, end, Math.random())
          // TODO: Spawn VerticalMover
        } else {
          pos.y += (section.top ? -1 : 1) * section.verticalOffset
          var flipTop = section.top && section.verticalOffset > 0
          var flipV = section.top && !flipTop
          // All enemies which belong to the same section are put into a group
          temporaryEnemy = this.SpawnEnemy(pos, (flipTop ? i === 0 : i >= nbEnemies - 1) && !section.growing, flipV, false, false, section.growing ? delay : 0, section.horizontalSpeed, nbEnemies, i, false, bounds, j, false)
          // Enemy knows to which group it belongs. For this, a parameter is created in the enemy, called group.
          temporaryEnemy.group = enemyGroup
          enemyGroup.add(temporaryEnemy)
          if (section.growing) { delay += 1.5 }
        }

        // TODO: Spawn shadow
      }
    }

    // TODO: Replace checkpoints on obstacles

    if (Math.random() > 0.5 && section.distToNext >= 325) {
      this.SpawnCoins(new Vector3(pos.x + Math.randRange(124.999, section.distToNext - 200), Math.randRange(this.scene.cameras.main.centerY - 225, this.scene.cameras.main.centerY + 175), 0))
    }

    if (section.verticalMoveRange > 0) {
      pos.x = (0.55 * (section.nbColumns - 1))
      pos.y = 20 + 1
      pos.z = 2.3
      // TODO: Spawn range indicator
    } else if (section.horizontalSpeed > 0) {
      // Move checkpoint and shadow
    }

    // The animations and collisions of the Section are now set:
    const enemyCollider = window.global.gameScene.physics.add.collider(window.global.player, enemyGroup, window.hitEnemy)
    enemyGroup.enemyGroupCollider = enemyCollider
    enemyGroup.section = section
    return enemyGroup
  }

  update () {
    this.coinSpawner.update()
    this.enemySpawner.update()

    if (this.sectionsToSpawn.length === 0 && this.player.x > this.nextSection - this.scene.cameras.main.centerX - 400) {
      this.GenerateNext()
    }

    // Spawn next section
    if (this.sectionsToSpawn.length === 0) {
      return
    }

    // no spawn till the player is close enough to the end of the previous enemy spawn.
    if (this.player.x < this.nextSection - this.initialDistance - this.scene.cameras.main.width - 128) { return }

    var oldPos = this.nextSection

    var section = this.sectionsToSpawn.shift()

    this.sectionsToPlay.push(this.SpawnSection(section, new Vector3(this.nextSection, 0, 0)))
    this.nextSection += section.distToNext + (section.nbColumns - 1) * 128

    // if the initial position is the player position, then these distances are added to the initial player position.
    if (oldPos < this.nextMarker && this.nextSection > this.nextMarker) {
      // TODO: Spawn distance marker
      this.nextMarker += 25
    }

    if (oldPos < this.nextPickup && this.nextSection > this.nextPickup) {
      // if(player.isInvicible <= 0 && genIndex >= 2) {
      // TODO: Spawn pickups
      // }
      this.nextPickup += Math.randRange(35, 100)
    }
  }
}
