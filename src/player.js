import Phaser from 'phaser'

let frameTracker
let frameChange
export default class Player extends Phaser.Physics.Arcade.Sprite {
  constructor (scene, x, y) {
    super(scene, x, y, 'player')

    this.scene = scene
    this.x = x
    this.y = y

    // add to scene
    scene.add.existing(this)
    scene.physics.add.existing(this)

    // settings
    this.setCollideWorldBounds(true)
    this.body.setGravityY(300)
    this.setDepth(2)
    this.setDisplaySize(90, 135)

    // variables
    this.keys = this.scene.input.keyboard.createCursorKeys()
    frameTracker = 1
    frameChange = false

    this.jetObject = this.scene.add.sprite(0, 0, 'jet')
    this.jetObject.depth = 1
  }

  Movement () {
    // forward movement for player
    if (this.keys.space.isDown) {
      this.setVelocityX(window.global.playerVelocity)
    } else {
      this.setVelocityX(0)
    }

    // jumping for player
    if (this.y >= 700) {
      this.setVelocityY(-800)
    }

    this.jetObject.x = this.x - 150
    this.jetObject.y = this.y - 30
  }

  CreateAnimation () {
    // tracks when spacebar is pressed down
    this.scene.input.keyboard.on('keydown-SPACE', function (event) {
      frameChange = true // trigger for forward jumping animation to switch frame
    })

    // tracks when spacebar is released
    this.scene.input.keyboard.on('keyup-SPACE', function (event) {
      frameChange = true // trigger for idle jumping animation to switch frame
    })

    // idle jump animation
    this.scene.anims.create({
      key: 'player_idle',
      frameRate: 9.1,
      // @ts-ignore
      frames: this.scene.anims.generateFrameNames('idle', {
        prefix: 'idle',
        suffix: '.png',
        start: 0,
        end: 24
      }),
      repeat: 0
    })

    // forward jump animation
    this.scene.anims.create({
      key: 'player_jumpforward',
      frameRate: 9.1,
      // @ts-ignore
      frames: this.scene.anims.generateFrameNames('jumpforward', {
        prefix: 'JF',
        suffix: '.png',
        start: 0,
        end: 24
      }),
      repeat: 0
    })

    // jet animation
    this.scene.anims.create({
      key: 'player_jetregular',
      frameRate: 9.1,
      // @ts-ignore
      frames: this.scene.anims.generateFrameNames('jetregular', {
        prefix: 'JB',
        suffix: '.png',
        start: 0,
        end: 17
      }),
      repeat: -1
    })
  }

  PlayAnimation () {
    if (this.keys.space.isDown) {
      this.anims.play('player_jumpforward', true)
      this.jetObject.alpha = 1
      this.jetObject.anims.play('player_jetregular', true)

      // starts from frame where jumping animation left off
      if (frameChange) {
        this.anims.setCurrentFrame(this.anims.currentAnim.frames[frameTracker - 1])
      }
      frameChange = false
    } else {
      this.anims.play('player_idle', true)
      this.jetObject.alpha = 0
      this.jetObject.anims.stop()

      // starts from frame where idle animation left off
      if (frameChange) {
        this.anims.setCurrentFrame(this.anims.currentAnim.frames[frameTracker - 1])
      }
      frameChange = false
    }
    frameTracker = this.anims.currentFrame.index
  }
}
