import Phaser from 'phaser'

export default class Coin extends Phaser.Physics.Arcade.Sprite {
  constructor (scene, isSuper = false) {
    var spriteKey = isSuper ? 'coin-super' : 'coin-normal'
    var animKey = isSuper ? 'coin-super-anim' : 'coin-normal-anim'
    super(scene, 400, 200, spriteKey)
    this.setScale(0.4)

    this.originalY = 200
    this.value = isSuper ? 5 : 1
    this.timeOffset = 0
    this.counter = 0

    scene.add.existing(this)
    this.anims.play(animKey)
    this.decimal = 0
  }

  setIndex (i) {
    this.index = i
  }

  setIsSuper (isSuper) {
    this.value = isSuper ? 5 : 1
    var animKey = isSuper ? 'coin-super-anim' : 'coin-normal-anim'
    this.anims.play(animKey)
  }

  setAnimationDelay (delay) {
    this.anims.setProgress(1000 * delay / this.anims.duration)
  }

  preUpdate (time, delta) {
    this.anims.update(time, delta)
    var t = (time - this.timeOffset) * this.anims.duration / (this.anims.msPerFrame * this.anims.currentAnim.frames.length * 200)
    this.y = this.originalY + 15 * Math.cos(t)
    this.rotation = 0.33166156 * Math.sin(t + 0.3)
  }
}
