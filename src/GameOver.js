import Phaser from 'phaser'

export default class GameOver extends Phaser.Scene {
  constructor () {
    super('game-over')
  }

  preload () {
  }

  create () {
    this.add.text(this.scale.width / 2 - 200, this.scale.height / 2, 'Game over, goodbye')
    window.global.keyboard = this.input.keyboard.createCursorKeys()
  }

  update () {
    // We declare the variable in GlobVars and implement it in MainMenue
    if (window.global.keyboard.space.isDown) {
      this.scene.start('main-menu')
    }
  }
}
