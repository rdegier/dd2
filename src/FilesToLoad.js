
export const initialLoadfilesGameScene = []
initialLoadfilesGameScene.push(['image', 'player', 'player.png', 'none'])
// texture atlases (for animations)
initialLoadfilesGameScene.push(['atlas', 'idle', 'Spritesheets/idle.png', 'Spritesheets/idle.json'])
initialLoadfilesGameScene.push(['atlas', 'jumpforward', 'Spritesheets/JumpForward.png', 'Spritesheets/JumpForward.json'])
initialLoadfilesGameScene.push(['atlas', 'jetregular', 'Spritesheets/JetRegular.png', 'Spritesheets/JetRegular.json'])
initialLoadfilesGameScene.push(['image', 'enemy_0', 'EnemySpawner/Enemy0_vertical.png', 'none'])
// The spriteatlas and the .json for the enemyanimation.
initialLoadfilesGameScene.push(['atlas', 'enemy0', 'Spritesheets/Enemies/Enemy0_sprite.png', 'Spritesheets/Enemies/Enemy0_sprite.json'])
initialLoadfilesGameScene.push(['atlas', 'coin-normal', 'Spritesheets/coin_normal.png', 'Spritesheets/coin_normal.json'])
initialLoadfilesGameScene.push(['atlas', 'coin-super', 'Spritesheets/coin_super.png', 'Spritesheets/coin_super.json'])
initialLoadfilesGameScene.push(['image', 'star', 'EnemySpawner/star.png', 'none'])
// background generator
initialLoadfilesGameScene.push(['image', 'sky', 'BackgroundGenerator/Background_Sky.png', 'none'])
initialLoadfilesGameScene.push(['image', 'clouds', 'BackgroundGenerator/SPR_BG_Clouds.png', 'none'])
initialLoadfilesGameScene.push(['image', 'frontfloor', 'BackgroundGenerator/frontfloor.png', 'none'])
initialLoadfilesGameScene.push(['image', 'hills', 'BackgroundGenerator/SPR_BG_Hills.png', 'none'])
initialLoadfilesGameScene.push(['image', 'moon', 'BackgroundGenerator/SPR_BG_Moon.png', 'none'])
initialLoadfilesGameScene.push(['image', 'mountain', 'BackgroundGenerator/mountain1.png', 'none'])
initialLoadfilesGameScene.push(['image', 'rock1', 'BackgroundGenerator/SPR_BG_Rock01.png', 'none'])
initialLoadfilesGameScene.push(['image', 'rock2', 'BackgroundGenerator/SPR_BG_Rock02.png', 'none'])
initialLoadfilesGameScene.push(['image', 'cloud1', 'BackgroundGenerator/SPR_Cloud01.png', 'none'])
initialLoadfilesGameScene.push(['image', 'cloud2', 'BackgroundGenerator/SPR_Cloud02.png', 'none'])
initialLoadfilesGameScene.push(['image', 'cloud3', 'BackgroundGenerator/SPR_Cloud03.png', 'none'])

// type, key, spritesheet, jsonsheet
export const progressiveLoadfiles = []
progressiveLoadfiles.push(['atlas', 'enemyfeet', 'Spritesheets/Enemies/Enemyfeet_wall_sprite.png', 'Spritesheets/Enemies/Enemyfeet_wall_sprite.json'])
progressiveLoadfiles.push(['image', 'ewallleftright', 'Spritesheets/Enemies/EWall_LeftRight.png', 'none'])
progressiveLoadfiles.push(['image', 'ewalltopbottom', 'Spritesheets/Enemies/EWall_BottomTop.png', 'none'])
progressiveLoadfiles.push(['atlas', 'enemy3', 'Spritesheets/Enemies/Enemy3_wall_sprite.png', 'Spritesheets/Enemies/Enemy3_wall_sprite.json'])
progressiveLoadfiles.push(['atlas', 'enemy2', 'Spritesheets/Enemies/Enemy2_sprite_wall.png', 'Spritesheets/Enemies/Enemy2_sprite_wall.json'])
progressiveLoadfiles.push(['atlas', 'enemy1Upsidedown', 'Spritesheets/Enemies/Enemy1_sprite_upsidedown.png', 'Spritesheets/Enemies/Enemy1_sprite_upsidedown.json'])
progressiveLoadfiles.push(['atlas', 'enemy0Upsidedown', 'Spritesheets/Enemies/Enemy0_sprite_upsidedown.png', 'Spritesheets/Enemies/Enemy0_sprite_upsidedown.json'])
// loadfiles.push(['image', 'img2', 'Spritesheets/Enemies/Background2.png', 'none'])
progressiveLoadfiles.push(['atlas', 'enemy1', 'Spritesheets/Enemies/Enemy1_sprite.png', 'Spritesheets/Enemies/Enemy1_sprite.json'])
progressiveLoadfiles.push(['image', 'ewalltopbottom', 'Spritesheets/Enemies/EWall_BottomTop.png', 'none'])

// game over screen
// progressiveLoadfiles.push(['image', 'crashedpopup', 'GameOverScreen/crashed_popupwindow.png', 'none'])
// progressiveLoadfiles.push(['image', 'surroundingwindow', 'GameOverScreen/surroundingWindow.png', 'none'])
// progressiveLoadfiles.push(['image', 'revivetext', 'GameOverScreen/ReviveText.png', 'none'])
// progressiveLoadfiles.push(['image', 'GameOverScreen/LifeButton.png', 'none'])
// progressiveLoadfiles.push(['image', 'lifebuttonhighlighted', 'GameOverScreen/LifeButtonHighlighted.png', 'none'])
// progressiveLoadfiles.push(['image', 'nextbutton', 'GameOverScreen/NextButton.png', 'none'])
// progressiveLoadfiles.push(['image', 'nextbuttonhighlighted', 'GameOverScreen/NextButtonHighlighted.png', 'none'])
// progressiveLoadfiles.push(['image', 'heartfishicon', 'GameOverScreen/icon_heartfish.png', 'none'])
