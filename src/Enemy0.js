
import Phaser from 'phaser'

// Reminder:
export default class Enemy0 {
  // Every Enemy knows it Index in the Array and the scene to belong to.
  constructor (sceneIn, index) {
    this.sceneForEnemy = sceneIn
    this.IndexEnemy = index
    this.isCollided = false
  }

  LoadEnemy () {
    // The placeholder-image for the enemyanimation.
    this.sceneForEnemy.load.image('enemy_0', 'bomb.png')
    // The spriteatlas and the .json for the enemyanimation.
    this.sceneForEnemy.load.atlas('enemy0', 'Spritesheets/Enemies/Enemy0_sprite.png', 'Spritesheets/Enemies/Enemy0_sprite.json')
    this.sceneForEnemy.load.atlas('idle', 'Spritesheets/idle.png', 'Spritesheets/idle.json')
  }

  // A new physical Entity is created. This Entity will belong tho the Object.
  CreatePhysicalEnemy () {
    // The image will be added as sprite. An global animation can now be loaded
    // into this sprite by .play.
    this.physicalEnemy = this.sceneForEnemy.physics.add.sprite(900, 700, 'enemy_0')
    this.physicalEnemy.setCollideWorldBounds(true)
    this.physicalEnemy.body.setGravityY(300)
    this.physicalEnemy.body.moves = false

    // Collision between enemy and dummy is created.
    const collisionBody = window.global.player.player
    this.sceneForEnemy.physics.add.collider(collisionBody, this.physicalEnemy, hitEnemy)

    // Create idle-turn animation
    // idle jump animation
    // Animations are globally created and can be accessed by any sprite afterwards.
    // The subsequent animation can therefore be accessed also by other gameObjects than physicalEnemy.
    // idle jump animation
    this.sceneForEnemy.anims.create({
      key: 'playEnemy',
      frameRate: 9.1,
      // @ts-ignore
      frames: this.sceneForEnemy.anims.generateFrameNames('enemy0', {
        // For the Prefix take the names as defined in the atlas .json-script
        prefix: 'Body_Vertical_',
        suffix: '.png',
        start: 1,
        end: 6
      }),
      repeat: 0
    })
  }

  GetPhysicalEnemy () {
    return this.physicalEnemy
  }

  GetEnemyIndex () {
    return this.indexEnemy
  }

  PlayEnemy () {
    // The global playEnemy animation is loaded.
    this.physicalEnemy.anims.play('playEnemy', true)
  }

  MoveEnemy (physicalEnemy, positionx, positiony) {
    physicalEnemy.x = positionx
    physicalEnemy.y = positiony
  }

  ActivateEnemy (physicalEnemy) {

  }

  DeactivateEnemy (physicalEnemy) {

  }
}

// The Callbackfunction has to be defined locally, since it cannot have variables in the Parantheses
// When it is called in the isCollided-Variable.
function hitEnemy () {
  // Here for example fitting methods of dummy and
  // player could be called.
  console.log('Collision')
  window.global.gameOver = true
}
