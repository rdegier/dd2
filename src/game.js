import './GlobalVars'
import './FilesToLoad'
import './Loader'
import './ProgressiveLoader2'
import Phaser from 'phaser'
import MainMenu from './MainMenu'
import LoadingScreen from './LoadingScreen'
import GameOver from './GameOver'
import GameScene from './GameScene'

var config = {
  type: Phaser.AUTO,
  width: 1380,
  height: 820,
  scale: {
    mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH
  },
  physics: {
    default: 'arcade',
    arcade: {
      gravity: { y: 300 },
      debug: true
    }
  },
  scene: [MainMenu,
    LoadingScreen,
    GameScene,
    GameOver]
}

export default new Phaser.Game(config)
