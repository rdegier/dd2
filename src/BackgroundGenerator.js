import Phaser from 'phaser'

var sky, moon, mountain
var playerTracker

var lowClouds = []
var nextCloud = 1024
var currentCloudIndex = 0

var ground = []
var nextGround = 1024
var currentGroundIndex = 0

var frontHills = []
var backHills = []
var numHills = 4
var hillResetRange = 5

var frontRocks = []
var backRocks = []
var rockResetRange = 20

var highClouds = []
var highCloudsScrollFac = 0.2

export default class BackgroundGenerator {
  constructor (sceneIn) {
    this.scene = sceneIn

    this.groundWidth = 1024
    this.cloudWidth = 1024
  }

  create () {
    const sceneWidth = this.scene.scale.width
    const sceneHeight = this.scene.scale.height

    // background sky
    sky = this.scene.add.image(sceneWidth / 2, sceneHeight / 2, 'sky')
    sky.setDisplaySize(sceneWidth + 100, sceneHeight)
    sky.setScrollFactor(0)

    // upper clouds
    highClouds[0] = this.scene.add.image(Phaser.Math.Between(sceneWidth / 2, sceneWidth * 2), Phaser.Math.Between(sceneHeight * 0.2, sceneHeight * 0.6), 'cloud1')
    highClouds[1] = this.scene.add.image(Phaser.Math.Between(sceneWidth / 2, sceneWidth * 2), Phaser.Math.Between(sceneHeight * 0.2, sceneHeight * 0.6), 'cloud2')
    highClouds[2] = this.scene.add.image(Phaser.Math.Between(sceneWidth / 2, sceneWidth * 2), Phaser.Math.Between(sceneHeight * 0.2, sceneHeight * 0.6), 'cloud3')

    for (var e = 0; e < 3; e += 1) {
      highClouds[e].setScrollFactor(highCloudsScrollFac)
    }

    // mountain
    mountain = this.scene.add.image(Phaser.Math.Between(sceneWidth / 2, sceneWidth * 2), sceneHeight * 0.47, 'mountain')
    mountain.setScrollFactor(0.15)

    // moon
    moon = this.scene.add.image(sceneWidth * 0.3, sceneHeight * 0.2, 'moon')
    moon.setDisplaySize(sceneWidth * 0.15, sceneHeight * 0.2)
    moon.setScrollFactor(0)

    // back rocks
    for (var c = 0; c < 4; c += 2) {
      backRocks[c] = this.scene.add.image(Phaser.Math.Between(0, sceneWidth * rockResetRange), this.scene.scale.height * 0.65, 'rock1')
      backRocks[c + 1] = this.scene.add.image(Phaser.Math.Between(0, sceneWidth * rockResetRange), this.scene.scale.height * 0.65, 'rock2')
    }

    // back hills
    for (var a = 0; a < numHills; a++) {
      backHills[a] = this.scene.add.image(Phaser.Math.Between(0, sceneWidth * hillResetRange), sceneHeight * 0.65, 'hills').setOrigin(0, 0.5)
    }

    // low clouds
    for (var i = 0; i < 3; i++) {
      if (i === 0) {
        lowClouds[i] = this.scene.add.image(nextCloud / 2, sceneHeight * 0.76, 'clouds')
      } else {
        lowClouds[i] = this.scene.add.image(lowClouds[i - 1].x + this.cloudWidth, sceneHeight * 0.76, 'clouds')
      }
    }

    // front rocks
    for (var d = 0; d < 4; d += 2) {
      frontRocks[d] = this.scene.add.image(Phaser.Math.Between(0, sceneWidth * rockResetRange), this.scene.scale.height * 0.75, 'rock1')
      frontRocks[d + 1] = this.scene.add.image(Phaser.Math.Between(0, sceneWidth * rockResetRange), this.scene.scale.height * 0.75, 'rock2')
    }

    // front hills
    for (var b = 0; b < numHills; b++) {
      frontHills[b] = this.scene.add.image(Phaser.Math.Between(0, sceneWidth * hillResetRange), sceneHeight * 0.75, 'hills').setOrigin(0, 0.5)
    }

    // ground
    for (var j = 0; j < 3; j++) {
      if (j === 0) {
        ground[j] = this.scene.add.image(nextGround / 2, sceneHeight * 0.94, 'frontfloor')
      } else {
        ground[j] = this.scene.add.image(ground[j - 1].x + this.groundWidth, sceneHeight * 0.94, 'frontfloor')
      }
    }
  }

  update () {
    // tracks the position of the player
    playerTracker = window.global.player.x - window.global.playerStartX

    this.SetFloor()
    this.SetClouds()
    this.SetHills()
    this.SetRocks()
    this.SetHighClouds()
  }

  // uses 3 floor pieces to generate the ground (last one goes to front based on player position)
  SetFloor () {
    if (playerTracker > nextGround) {
      if (currentGroundIndex === 0) {
        ground[currentGroundIndex].x = ground[2].x + this.groundWidth

        currentGroundIndex = 1
      } else if (currentGroundIndex === 1) {
        ground[currentGroundIndex].x = ground[0].x + this.groundWidth

        currentGroundIndex = 2
      } else {
        ground[currentGroundIndex].x = ground[1].x + this.groundWidth

        currentGroundIndex = 0
      }

      nextGround += this.groundWidth
    }
  }

  // uses 3 cloud pieces to generate the low clouds (last one goes to front based on player position)
  SetClouds () {
    if (playerTracker > nextCloud) {
      if (currentCloudIndex === 0) {
        lowClouds[currentCloudIndex].x = lowClouds[2].x + this.cloudWidth

        currentCloudIndex = 1
      } else if (currentCloudIndex === 1) {
        lowClouds[currentCloudIndex].x = lowClouds[0].x + this.cloudWidth

        currentCloudIndex = 2
      } else {
        lowClouds[currentCloudIndex].x = lowClouds[1].x + this.cloudWidth

        currentCloudIndex = 0
      }

      nextCloud += this.cloudWidth
    }
  }

  SetHills () {
    // front hills
    for (var i = 0; i < frontHills.length; i++) {
      if (frontHills[i].x < playerTracker - (window.global.playerCamOffSet * 2)) {
        var randomNum = Phaser.Math.Between(this.scene.scale.width * 1.5, this.scene.scale.width * hillResetRange)
        frontHills[i].x += randomNum
      }
    }

    // back hills
    for (var j = 0; j < backHills.length; j++) {
      if (backHills[j].x < playerTracker - (window.global.playerCamOffSet * 2)) {
        var randomNum2 = Phaser.Math.Between(this.scene.scale.width * 1.5, this.scene.scale.width * hillResetRange)
        backHills[j].x += randomNum2
      }
    }
  }

  SetRocks () {
    // front rocks
    for (var i = 0; i < frontRocks.length; i++) {
      if (frontRocks[i].x < playerTracker - (window.global.playerCamOffSet * 2)) {
        var randomNum = Phaser.Math.Between(this.scene.scale.width * 2, this.scene.scale.width * rockResetRange)
        frontRocks[i].x += randomNum
      }
    }

    // back rocks
    for (var j = 0; j < backRocks.length; j++) {
      if (backRocks[j].x < playerTracker - (window.global.playerCamOffSet * 2)) {
        var randomNum2 = Phaser.Math.Between(this.scene.scale.width * 2, this.scene.scale.width * rockResetRange)
        backRocks[j].x += randomNum2
      }
    }
  }

  SetHighClouds () {
    for (var i = 0; i < highClouds.length; i++) {
      if (highClouds[i].x / highCloudsScrollFac * 1.2 < playerTracker) {
        var randomNumX = Phaser.Math.Between(this.scene.scale.width * 1.3, this.scene.scale.width * 2.5)
        var randomNumY = Phaser.Math.Between(this.scene.scale.height * 0.2, this.scene.scale.height * 0.6)
        highClouds[i].x += randomNumX
        highClouds[i].y = randomNumY
      }
    }
  }
}
