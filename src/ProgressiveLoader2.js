
let type
let key
let spritesheet
let jsonsheet
let currentFile

// ['atlas', 'enemy0Upsidedown', 'Spritesheets/Enemies/Enemy0_sprite_upsidedown.png', 'Spritesheets/Enemies/Enemy0_sprite_upsidedown.json'],
// Don't load the files again, if they already exist.
function progressiveLoader2 (loadfilesIn) {
  if (loadfilesIn.length > 0) {
    currentFile = loadfilesIn.pop()
    // console.log('second entry: ' + currentFile[1])
    type = currentFile[0]
    key = currentFile[1]
    spritesheet = currentFile[2]
    jsonsheet = currentFile[3]
    // console.log('window loadfiles size: ' + loadfilesIn.length)
    // If the picture already exists, don't load it again.
    // This is useful, when we restart the game.
    if (!window.global.gameScene.textures.exists(key)) {
      switch (type) {
        case ('atlas'):
          window.global.gameScene.load.atlas(key, spritesheet, jsonsheet)
          window.global.gameScene.load.start()
          window.global.gameScene.load.on('complete', window.progressiveAdder2.bind(null, key))
          break
        case ('image'):
          // console.log('key background  ' + key)
          // console.log('link background  ' + spritesheet)
          window.global.gameScene.load.image(key, spritesheet)
          break
        case ('soundfile'):
          break
        default:
      }
    } else { console.log('Double Entry of: ' + spritesheet) }
  }
}
window.progressiveLoader2 = progressiveLoader2

// -----------------------------------------------------------------
// If pictures should be added somewhere at a fixed position, it can be done just after loading
function progressiveAdder2 (caseIn) {
  switch (caseIn) {
    case ('enemy0Upsidedown'):
      // console.log('create red animation')
      window.AnimationEnemy('enemy0Upsidedown', 'playEnemy0upside', 'EUpsidedown_Body_', '.png', 1, 6)
      window.AnimationEnemy('enemy0Upsidedown', 'playEnemyHead0upside', 'EUpsidedown_Head_', '.png', 1, 24)
      break
    case ('enemy1'):
      // function AnimationEnemy (key, playKey, prefix, suffix)
      // console.log('create blue animation')
      window.AnimationEnemy('enemy1', 'playEnemy1', 'Body_Vertical_Blue_', '.png', 1, 6)
      window.AnimationEnemy('enemy1', 'playEnemy1Head', 'Head_Vertical_Blue_', '.png', 1, 24)
      break
    case ('enemy1Upsidedown'):
      // function AnimationEnemy (key, playKey, prefix, suffix)
      // console.log('create blue animation')
      window.AnimationEnemy('enemy1Upsidedown', 'playEnemy1Upside', 'EUpsidedown_Blue_Body_', '.png', 1, 6)
      window.AnimationEnemy('enemy1Upsidedown', 'playEnemy1HeadUpside', 'EUpsidedown_Blue_Head_', '.png', 1, 24)
      break
    case ('enemy2'):
      window.AnimationEnemy('enemy2', 'playEnemy2', 'EWall_Head_', '.png', 1, 24)
      break
    case ('enemy3'):
      window.AnimationEnemy('enemy3', 'playEnemy3', 'EWall_Head_Blue_', '.png', 1, 24)
      break
    // default: console.log('default')
  }
}
window.progressiveAdder2 = progressiveAdder2
