
let type
let key
let spritesheet
let jsonsheet
let currentFile

// ['atlas', 'enemy0Upsidedown', 'Spritesheets/Enemies/Enemy0_sprite_upsidedown.png', 'Spritesheets/Enemies/Enemy0_sprite_upsidedown.json'],
// Don't load the files again, if they already exist.
function loader1 (loadfilesIn) {
  while (loadfilesIn.length > 0) {
    currentFile = loadfilesIn.pop()
    console.log('second entry: ' + currentFile[1])
    type = currentFile[0]
    key = currentFile[1]
    spritesheet = currentFile[2]
    jsonsheet = currentFile[3]
    console.log('window loadfiles size: ' + loadfilesIn.length)
    // If the picture already exists, don't load it again.
    // This is useful, when we restart the game.
    console.log('key ' + key + ' spritesheet ' + spritesheet + ' jsonsheet ' + jsonsheet)
    if (!window.global.gameScene.textures.exists(key)) {
      switch (type) {
        case ('atlas'):
          window.global.gameScene.load.atlas(key, spritesheet, jsonsheet)
          window.global.gameScene.load.start()
          console.log('key load ' + key + ' spritesheet load ' + spritesheet + ' jsonsheet load ')
          break
        case ('image'):
          console.log('key background  ' + key)
          console.log('link background  ' + spritesheet)
          window.global.gameScene.load.image(key, spritesheet)
          break
        case ('soundfile'):
          break
        default:
      }
    } else { console.log('Double Entry of: ' + spritesheet) }
  }
}
window.loader1 = loader1
