import Phaser from 'phaser'

export default class MainMenu extends Phaser.Scene {
  constructor () {
    super('main-menu')
  }

  preload () {
    this.load.image('background', '/MainMenu/Background.png')
    this.load.image('pogologo', '/MainMenu/pogologo2.png')
    this.load.image('controls', '/MainMenu/control_space.png')
  }

  create () {
    this.add.image(690, 410, 'background')
    this.add.image(250, 600, 'pogologo').setScale(0.5)
    this.add.image(1100, 725, 'controls').setScale(0.5)
    window.global.keyboard = this.input.keyboard.createCursorKeys()
  }

  update () {
    if (window.global.keyboard.space.isDown) {
      this.scene.start('loading-screen')
    }
  }
}
